# Reaction time tester
## Functional description
A simple reaction time tester for arduino.
As an indicator that test can begin, the green LED lights up
and the serial monitor prints "Reaction Tester ready! Press button 1"

After button 1 is pressed the green LED starts blinking 
and the serial monitor prints "Waiting for the red LED..."

After a random time between 1.5 and 5 seconds, the green LED tuns off and 
the red LED lights up.
Now the time until button 2 is pressed is measured.

After button 2 is pressed the green LED turns on, the red LED off
and the serial monitor prints the reaction time in seconds.

## Hardware requirements
- Arduino (Uno)
- USB cable (for Arduino)
- Breadboard
- Green LED
- Red LED
- 2 x button
- 2 x 220Ω resistor 
- 7 connection wires

## Circuit diagram
![](Circuirt.png)

## Hardware setup

![](HardwareSetup.png)

## Code explanation
The most important parts of the code are explained below.

---
#### The blink function
```cpp
void ledBlink1(int pLED, int pTime) {
  int stateLED = digitalRead(pLED);
  if (currentTime - timeLastSwitch1 >= pTime)
    {  
    digitalWrite(pLED, !stateLED);
    timeLastSwitch1 = currentTime;
    }
}
```
As you can see, the function takes two parameters. The first is the pin number of the LED
and the second is the interval time in milliseconds.

The function checks if the current time is greater than (longer ago than) the time of the last switch
and if so, the LED is switched on or off.
After that, the timeLastSwitch is set to the current time.

Switching the LED on or off is done by using the "!" operator which inverts the state of the LED.

---
#### The reaction time function
```cpp
void reaction() {
  if (reactionSetup == 0) { // Checks if reactionSetup is equal to zero (to ensure this if-branch is only executed once)
    digitalWrite(LED_RED, HIGH);
    digitalWrite(LED_GRN, LOW);
    startTime = millis(); // Sets the startTime variable to the current time (used to calculate the reaction time)
    reactionSetup = 1; // Sets the reactionSetup variable to 1, to only go through this if-branch once
  }
  b2.update(); // Update the Bounce instance
  if ( b2.fell() ) { // Call code if button is pressed
    double result = (millis() - startTime); // Calculates the reaction time
    double resInSec = result / 1000; // Converts the result to seconds
    Serial.print("Reaction time: "); // Serial monitor prints the result from the test
    Serial.print(resInSec);
    Serial.println(" seconds");
    Serial.println("---------------------------------------------------");
    digitalWrite(LED_RED, LOW); // Turns the red LED off
    state = 4 ; // Gives the state variable the value 4
  }
}
```
This function is used to calculate the reaction time, next to the program specific actions
(e.g. turning the red LED on and the green LED off).
The time is measured by the difference between the current time and the time when the function was called first.

To not overwrite the startTime variable, the if-branch at the beginning is set up to only execute once.

---
#### AntiCheat function
```cpp
void antiCheat() { 
  b2.update(); 
  if (b2.fell()) {  
    Serial.println("Stop Cheating"); 
    Serial.println("---------------------------------------------------");
    state = 6;
  }
}
```
This function is to prevent the user from cheating.
If button 2 is pressed too early, the serial monitor prints "Stop Cheating" and the state variable is set to 6
(to stop the test and flash both LEDs).

Without this function the user could cheat 
by pressing button 2 before the red LED is turned on (the time would be 0.03 seconds
because the button is pressed while entering the time measurement).

---
#### The switch case
```cpp
switch (state) { ...
``` 
The switch case is used to switch through the different program states.
The program is divided into 5 states, plus the initial state (state 0).

---
##### State 0
case 0: Not really a switch case but the state the program is in when it starts.

In this state the program prints the "welcome" message
after button 1 is pressed the "waiting for the red LED..." message is printed 
and the state is set to 1.

---
##### The state 1

```cpp
case 1:
    eventReactionStartTime = random(1500, 5000) + currentTime;
    state = 2;
```
This state is used to generate a random time between 1.5 and 5 seconds,
that is used have a random value until the red LED is turned on.

---
##### The state 2
```cpp
case 2:
      ledBlink1(LED_GRN, 300);
      if (currentTime == eventReactionStartTime){ 
        state = 3; 
      } 
      antiCheat(); 
      break;
```
This is the state where the program waits that the random time from case
1 is reached, to change the state to 3, while waiting the ledBlink function is called 
to let the green LED blink.

antiCheat function is used to check if the button is pressed too early.

---
##### The state 3
```cpp
case 3:
      reaction();
      break;
```
In this state the reaction function is called to start the reaction time measurement.
The function contains all the program steps (led on off and changing to the next state).

---
##### The state 4
```cpp
case 4: 
      digitalWrite(LED_GRN, HIGH);
      state = 5;
      break;
```
This is the last state of the program.
The green LED is turned on and the state is set to 5, to run in an empty endless loop.

---
##### The state 5
Was planed to implement a "play again" functionality.

---
##### The state 6
```cpp
case 6:
      ledBlink1(LED_RED, 300);
      ledBlink2(LED_GRN, 200); 
      break;
```
This state is entered when the user pressed button 2 too early in state 2.


