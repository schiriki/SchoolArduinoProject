/*!
 *Reaction time tester
 */

#include <Bounce2.h> //include bounce libraries

#define LED_RED 8 // Sets Pin 9 as placeholder
#define LED_GRN 9 // Sets Pin 8 as placeholder
#define BUT1 2 // Sets Pin 2 as placeholder
#define BUT2 4 // Sets Pin 4 as placeholder

Bounce b2 = Bounce();               // Create a new object called b2 from the datatype Bounce2

unsigned long currentTime = 0;      // Variable for the time the program is running
int state = 1;                      // Variable to indicate the current state of the program (used in the switch statement)

unsigned long timeLastSwitch1 = 0;  // Variable used by the ledBlink1 function
unsigned long timeLastSwitch2 = 0;  // Variable used by the ledBlink2 function 

int eventReactionStartTime;          // Random value that is used to trigger the reaction function

int reactionSetup = 0;              // Variable used to go once through the reactionSetup (e.g. to get the start time and not overwrite it)
unsigned long startTime;            // Variable used to calculate reaction time from the user

void ledBlink1(int pLED, int pTime) {           // Function to blink the LED at a certain time
  int stateLED = digitalRead(pLED);             // Gets the current state of the LED
  if (currentTime - timeLastSwitch1 >= pTime) // If the last switch was more than the time specified
    {  
    digitalWrite(pLED, !stateLED);             // Switch the LED
    timeLastSwitch1 = currentTime;            // Set the time the last switch was made
    }
} // Ends ledBlink1 function

void ledBlink2(int pLED, int pTime) { // Same as ledBlink1, for second LED (only used for antiCheat)
  int stateLED = digitalRead(pLED);
  if (currentTime - timeLastSwitch2 >= pTime)
  {
    digitalWrite(pLED, !stateLED);
    timeLastSwitch2 = currentTime;
  }
} // Ends ledBlink2 function

/*Function that can be called to check if button 2 is pressed before the RED LED turns on */
void antiCheat() {  // Creates a function named antiCheat
  b2.update();      // Update the Bounce instance
  if (b2.fell()) {  // Call code if button is pressed
    Serial.println("Stop Cheating"); // Serial monitor prints "Stop Cheating"
    Serial.println("---------------------------------------------------"); // Serial monitor prints "---------------------------------------------------"
    state = 6; // Gives the state variable the value 6 to go in the cheat state
  }
} //Ends antiCheat function

/* Function that is used to measure the time between the Red LED turning on and the button being pressed */
void reaction() { // Creates a function named reaction
  if (reactionSetup == 0) { // Checks if reactionSetup is equal to zero (to ensure this if-branch is only executed once)
    digitalWrite(LED_RED, HIGH); // Turns the RED LED on
    digitalWrite(LED_GRN, LOW); // Turns the GREEN LED off
    startTime = millis(); // Sets the startTime variable to the current time (used to calculate the reaction time)
    reactionSetup = 1; // Sets the reactionSetup variable to 1, to only go through this if-branch once
  }
  b2.update(); // Update the Bounce instance
  if ( b2.fell() ) { // Call code if button is pressed
    double result = (millis() - startTime); // Calculates the reaction time
    double resInSec = result / 1000; // Converts the result to seconds
    Serial.print("Reaction time: "); // Serial monitor prints the result from the test
    Serial.print(resInSec);
    Serial.println(" seconds");
    Serial.println("---------------------------------------------------"); // Serial monitor prints "---------------------------------------------------"
    digitalWrite(LED_RED, LOW); // Turns the red LED off
    state = 4 ; // Gives the state variable the value 4
  }
} //Ends the reaction function

void setup() { // Starts the setup function, that is only running once
  Serial.begin(9600); // Starts the serial monitor with a bitrate of 9600
  randomSeed(analogRead(0)); // generates a random seed for the random function
  pinMode(LED_RED, OUTPUT); //defines the "LED_RED" pin as out
  pinMode(LED_GRN, OUTPUT); //defines the "LED_GRN" pin as out
  pinMode(BUT1, INPUT_PULLUP); //defines the BUT1 pin as input  (with pullup resistor)
  b2.attach(BUT2, INPUT_PULLUP); //defines the BUT2 pin as input (with pullup resistor)
  b2.interval(25); // Sets the interval of the Bounce instance to 25ms
} // Ends the setup function

void loop() { // Starts the loop function, that is running multiple times
  currentTime = millis(); // Sets the currentTime variable to the current time
  switch (state) { // Starts a switch-statement, that is checking the state variable
    case 1: // Starts the case 1-branch
        digitalWrite(LED_GRN, HIGH); // Turns the green LED on
        Serial.println("---------------------------------------------------"); // Serial monitor prints "---------------------------------------------------"
        Serial.println("Reaction Tester ready! Press button 1."); // Serial monitor prints "Reaction Tester ready! Press button 1."

        while (digitalRead(BUT1) == 1) // Checks if button 1 is pressed (so the user has to press it before the program starts)
        {
        }
        eventReactionStartTime = 0;
        Serial.println("Waiting for the red light …"); // Serial monitor prints "Waiting for the red light …"
        digitalWrite(LED_GRN, LOW); // Turns the green LED off
        reactionSetup = 0;
      currentTime = millis();
      eventReactionStartTime = random(2000, 5000) + currentTime;  // Sets the eventReactionStartTime variable to a random value between 1500 and 5000 plus the currentTime
      state = 2; // Gives the state variable the value 2
      break; // Ends the case 1-branch
    case 2: //  Starts the case 2-branch
      ledBlink1(LED_GRN, 300); // Calls the ledBlink1 function with the pin from the green LED and the flash length of 300ms
      currentTime = millis();
      if (currentTime == eventReactionStartTime){ // Checks if the currentTime is equal to the eventReactionStartTime
        state = 3; // Gives the state variable the value 3 to go in the reaction state
      } // Ends the if-branch
      antiCheat(); // Calls the antiCheat function to check if the button is pressed before the red LED turns on
      break; // Ends the case 2-branch
    case 3: // Starts the case 3-branch
      reaction(); // Calls the reaction function (start reaction time measurement)
      break; // Ends the case 3-branch
    case 4: // Starts the case 4-branch
      digitalWrite(LED_GRN, HIGH); // Turns the LED_GRN pin on
      delay(500);
      state = 5; // Gives the state variable the value 5
      break; // Ends the case 4-branch
    case 5:
      if(digitalRead(BUT1) == 0){
        digitalWrite(LED_RED, LOW);
        digitalWrite(LED_GRN, LOW);
        state = 1;
      }
      break;
    case 6: // Starts the case 6-branch (only used for the anti-cheat)
      ledBlink1(LED_RED, 300); // Calls the ledBlink1 function with the pin from the red LED and a flash length of 300ms
      ledBlink2(LED_GRN, 200); // Calls the ledBlink2 function with the pin from the green LED and a flash length of 200ms
      break; // Ends the case 6-branch
  }  // Ends the switch-statement
} // Ends the loop function
